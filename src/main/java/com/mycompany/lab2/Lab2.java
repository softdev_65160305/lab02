/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab2;

/**
 *
 * @author ADMIN
 */
import java.util.Scanner;
import java.util.HashSet;
import java.util.Set;
public class Lab2 {
    static String number;
    static boolean gameOver = false;
    static Set<String> chosenNumber = new HashSet<>();
    static void printWelcome(){
    System.out.println("Welcome to XO");
}
    static String[][] matrix =new String[3][3];
    static{
            matrix[0][0] = "1";
        matrix[0][1] = "2";
        matrix[0][2] = "3";
        matrix[1][0] = "4";
        matrix[1][1] = "5";
        matrix[1][2] = "6";
        matrix[2][0] = "7";
        matrix[2][1] = "8";
        matrix[2][2] = "9";
    }
    
    static void printTable(){
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                System.out.print(matrix[i][j]+" ");
            }
            System.out.println();
        }
    }
    static String currentplayer= "X";
    static void printTurn(){
        System.out.println("Turn "+currentplayer);
    }
    
    static void inputNumber(){
        Scanner num = new Scanner(System.in);
        System.out.print("Plase input your Number: ");
        number = num.next() ;
        checkRepeat();
        setTable(number);
        }
        
    static void setTable(String number){
        if(number.equals("1")){
            matrix[0][0] = currentplayer;
        } else if (number .equals("2")) {
            matrix[0][1] = currentplayer;
        } else if (number .equals("3")) {
            matrix[0][2] = currentplayer;
        } else if (number .equals("4")) {
            matrix[1][0] = currentplayer;
        } else if (number .equals("5")) {
            matrix[1][1] = currentplayer;
        } else if (number .equals("6")) {
            matrix[1][2] = currentplayer;
        } else if (number .equals("7")) {
            matrix[2][0] = currentplayer;
        } else if (number .equals("8")) {
            matrix[2][1] = currentplayer;
        } else if (number .equals("9")) {
            matrix[2][2] = currentplayer;
        }
    }
    static void switchplayer(){
        if(currentplayer=="X"){
            currentplayer="O";
        }
        else{
            currentplayer="X";
        }
    }
    static boolean checkwin(){
        //horizontal
        if(matrix[0][0] == currentplayer && matrix[0][1] == currentplayer && matrix[0][2] == currentplayer){
            return true;
        }else if(matrix[1][0] == currentplayer && matrix[1][1] == currentplayer && matrix[1][2] == currentplayer){
            return true;
        }else if(matrix[2][0] == currentplayer && matrix[2][1] == currentplayer && matrix[2][2] == currentplayer){
            return true;
            //vertical
        }else if(matrix[0][0] == currentplayer && matrix[1][0] == currentplayer && matrix[2][0] == currentplayer){
            return true;
        }else if(matrix[0][1] == currentplayer && matrix[1][1] == currentplayer && matrix[2][1] == currentplayer){
            return true;
        }else if(matrix[0][2] == currentplayer && matrix[1][2] == currentplayer && matrix[2][2] == currentplayer){
            return true;
            //Diagonal
        }else if(matrix[0][0] == currentplayer && matrix[1][1] == currentplayer && matrix[2][2] == currentplayer){
            return true;
        }else if(matrix[0][2] == currentplayer && matrix[1][1] == currentplayer&& matrix[2][0] == currentplayer){
            return true;
        }
        return false;
    }
    static boolean Iswin(){
        if(checkwin()){
            return true;
        }
        else{
            return false;
        }}  
    static void printwin(){
            System.out.println(currentplayer + " Is Win!!!");
        } 
    static boolean checkdraw(){
        if(matrix[0][0] == "1"|| matrix[0][1] == "2" || matrix[0][2] == "3"|| matrix[1][0] == "4" ||matrix[1][1] == "5" ||matrix[1][2] == "6" ||matrix[2][0] == "7" ||matrix[2][1] == "8" ||matrix[2][2] == "9"){
        return false;}
        else{
            return true;
        }
    }
    static boolean Isdraw(){
        if(checkdraw()){
            return true;
        }
        else{
            return false;
        }
    }
    static void printdraw(){
        System.out.println("Draw!!!");
    }
    static void regame(){
        System.out.print("Do you want to play again(Y/N)? : ");
        Scanner kb =new Scanner(System.in);
        String playAgain =kb.next();  
        if(playAgain.equals("Y")){
            resetgame();
    }}
    static void resetgame(){
        chosenNumber.clear();
        gameOver=false;
        currentplayer="X";
        matrix =new String[3][3];
        matrix[0][0] = "1";
        matrix[0][1] = "2";
        matrix[0][2] = "3";
        matrix[1][0] = "4";
        matrix[1][1] = "5";
        matrix[1][2] = "6";
        matrix[2][0] = "7";
        matrix[2][1] = "8";
        matrix[2][2] = "9";
    }
    static void checkRepeat(){
        while (chosenNumber.contains(number)) {
            printTable();
            printTurn();
            inputNumber();
            }
        chosenNumber.add(number); 
    }

    public static void main(String[] args) {
        printWelcome();
        while(true){
            printTable();
            printTurn();
            inputNumber();
            if(Iswin()){
                setTable(number);
                printwin();
                gameOver = true;
                if(gameOver == true){
                    regame();
                    continue;
                }
            break;
            }         
            if(Isdraw()){
            setTable(number);
            printdraw();
            gameOver = true;
                if(gameOver == true){
                    regame();
                    continue;
            }
            break;
            }
            switchplayer();
            }}}


